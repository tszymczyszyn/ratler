Usage:

    Ratler::Client.setup({ 
      :facebook => { :limit => 600, :span => 600 },
      :youtube => { :limit => 10_000, :span => 24 * 60 * 60 }
    })

    client = Ratler::Client.new(:facebook) do |*args|
      Facebook.doStuff(*args)
    end

    client.call(1, "abc") # => returns whatever Facebook call returns

