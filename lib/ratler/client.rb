# -*- encoding: utf-8 -*-
module Ratler
  class UnrecognizedApi < ArgumentError; end
  class LimitExceeded < RuntimeError; end

  class Client
    def self.setup(config)
      @@config = config
    end

    def initialize(api_name, api_client, &block)
      raise UnrecognizedApi, "API named #{api_name} not configured" unless @@config.key?(api_name)
      @api_name = api_name
      @api_client = api_client
      @counter = Counter.new(api_name, @@config[api_name][:limit], @@config[api_name][:span])
    end

    def call(method_name, *args, &block)
      raise LimitExceeded, "Limit for #{@api_name} exceeded" unless @counter.free?
      @api_client.send(method_name, *args, &block)
    end
  end
end
