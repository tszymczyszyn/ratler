# -*- encoding: utf-8 -*-
module Ratler
  class Counter

    def initialize(name, limit, span)
      @name = name
      @limit = limit.to_i
      @span = span.to_i
    end

    def free?
      current_key = key
      return redis.multi do
        redis.incr(current_key)
        redis.expire(current_key, @span)
      end.first <= @limit
    end

    private

    def redis
      return @redis ||= Redis.new
    end

    def key
      bucket = Time.now.to_i / @span
      return "ratler:#{@name}:#{@limit}:#{@span}:#{bucket}"
    end
  end
end
