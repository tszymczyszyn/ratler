# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name            = "ratler"
  s.version         = "0.0.1"
  s.platform        = Gem::Platform::RUBY
  s.authors         = ["Tomasz Szymczyszyn"]
  s.email           = ["tomasz.szymczyszyn@gmail.com"]
  s.summary         = ""
  s.require_paths   = ['lib']
  s.files           = `git ls-files`.split($\)
  s.test_files      = s.files.grep(%r{^test/})

  s.required_ruby_version = ">= 1.8.7"
  s.required_rubygems_version = ">= 1.3.6"

  s.add_runtime_dependency "redis"
  s.add_development_dependency "rake"
  s.add_development_dependency "bundler"
  s.add_development_dependency "rspec"
  s.add_development_dependency "delorean"
  s.add_development_dependency "mock_redis"
end

