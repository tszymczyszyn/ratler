require "spec_helper"

describe Ratler::Client do
 
  it "call real client when rate is not exceeded" do
    facebook = double
    facebook.should_receive(:do_stuff).with("123")

    Ratler::Client.setup(:facebook => { :limit => 5, :span => 60 })
    client = Ratler::Client.new(:facebook, facebook)

    client.call(:do_stuff, "123")
  end

  it "raise exception when rate is exceeded" do
    facebook = double
    facebook.should_not_receive(:do_stuff)

    Ratler::Client.setup(:facebook => { :limit => 0, :span => 60 })
    client = Ratler::Client.new(:facebook, facebook)

    expect { client.call(:do_stuff, "123") }.to raise_error(Ratler::LimitExceeded)
  end

end

