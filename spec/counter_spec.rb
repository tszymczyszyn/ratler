require "spec_helper"

describe Ratler::Counter do

  context "#free?" do

    after(:each) { Delorean.back_to_the_present }

    it "returns false / true when rate exceeds / does not exceed the limit" do
      counter = Ratler::Counter.new("foo", 5, 60)
      5.times { counter.free?.should be_true }
      counter.free?.should be_false

      Delorean.jump(60)

      5.times { counter.free?.should be_true }
      counter.free?.should be_false
    end
  end
end

